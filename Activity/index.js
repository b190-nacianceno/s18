console.log("Hello world");

// function that adds numbers
function addNum(num1,num2){
    console.log(num1+num2);
}
addNum(5,2);

// function that subtracts numbers
function subtractNum(num1,num2){
    console.log(num1-num2);
}
subtractNum(10,2);

// function that multiplies numbers

function multiplyNum (num1,num2){
    return num1*num2;
}
let product = multiplyNum(5,10);
console.log(product);

// function that divides numbers

function divideNum (num1,num2){
    return num1/num2;
}
let quotient = divideNum(49,7);
console.log(quotient);

// function that find the area of circle, given the radius

function getAreaCircle (radius) {
    return 3.14*(radius**2);
}

let circleArea = getAreaCircle(9);
console.log(circleArea);

// function that gets the average of four numbers

function getAverageFour (num1,num2,num3,num4){
    return (num1+num2+num3+num4)/4;
}
let averageVar = getAverageFour(12,10,15,90);
console.log(averageVar);


// function that checks if the score passes the passing percentage
function passingScore(score,totalscore){
    let isPassed = (score/totalscore)*100;
    return isPassed > 75;
}

let isPassingScore = (passingScore(39,50));
console.log(isPassingScore);



