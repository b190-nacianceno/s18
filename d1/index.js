/* 
Functions are lines/blocks of codes that tell our devices/application to perform certain tasks when called/invoked.

Functions are mostly created to create complicated tasks to run several lines of codes in succession. 

They are used to prevent repeating codes that do the same task.
*/

/* Mini activity
create a function
    creat a let var that is called nickname, the value should come from a prompt();

    log in the consol the var

    invoke
*/

// function nickName(){
//     let nickname = prompt("What is your nickname?");
//     console.log(nickname);
// }

// nickName();


// However, for some cases, this may not be ideal. For other cases, functions can also process data directly passed into it instead of relying on global variables such as promtpt();

// Consider this function:
// we can directly pass data into the function. For the function below, the data is reffered to as 'name' in the function. This name is what we called as the parameter.

/* 
PARAMETER (name) - acts as a named variable/container that exists only inside of a function;
-used to store information that will be used inside the function,
-used to store information that is provided to a function when it is called/invoked

*/

function printName(name){
    console.log ("Hello " + name);
};
// ARGUMENT "Joana" - the information provided directly into the function is called an argument. Values passed when invoking a function are called arguments. These arguments are then stored inside the parameters within the function
printName("Joana");

// these two are both arguments since both of them are supplied as information that will be used to print the message of the function. John and Jane are stored in the parameter 'name' then the function prints the message
printName("John");
printName("Jane");

let sampleVar = "Yua";
printName(sampleVar);


/* 
Mini activity
create 2 functions
    1st function should have 2 parameters
        log in the console the message "the numbers passed as arguments are:"
        log in the console the first parameter (number)
        log in the console the second parameter (number)
    invoke the function
*/

function numbers (num1,num2){
    console.log("The numbers passed as arguments are: " + num1 + " and " + num2);
}

numbers("1","2");


function myFriends (friend1, friend2, friend3){
    console.log("My friends are " + friend1 + ", " + friend2 + ", and " + friend3);
}

myFriends("Chia","Daphne","Doria");


// improving our codes for the last activity with the use of parameters
function checkDivisibilityBy8(num){
    let remainder = num%8;
    console.log("The remainder of " + num + " is " + remainder);

    let isDivisibleBy8 = remainder === 0;
    console.log("Is " + num + " divisible by 8?");
    console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);
// We can also do the same using prompt(). However, using prompt() outputs a string data type. String data types cannot be used for mathematical computations.


function checkDivisibilityBy4(num1){
    let remainderFour = num1%4;
    console.log("The remainder of " + num1 + " is " + remainderFour);

    let isDivisibleBy4 = remainderFour === 0;
    console.log("Is " + num1 + " divisible by 4?");
    console.log(isDivisibleBy4);
}
checkDivisibilityBy4(28);
checkDivisibilityBy4(25);


// Debug
function isEven(num){
	let numEven = num%2 === 0;
    console.log(numEven);
}

function isOdd(num1){
	let numOdd = num1%2 !== 0;
    console.log(numOdd);
}

isEven(20);
isOdd(31);

// console.log(isEven);
// console.log(isOdd);


/* 
FUNCTIONS ARE ARGUMENTS
    function parameters can also accept other functions as arguments
    some complex functinos use other functions as arguments to perform complicated tasks/result
        This will be further discussed when we tackle array methods
*/

function argumentFunction(){
    console.log("This function is passed into another function.");

};

function invokeFunction(functionParameter){
    functionParameter();
}

// adding and removing of the parenthesis impacts the output of JS heavily. when a function is used with parenthesis, it denotes that invoking/calling a function - that is why when we call the function, the functionParameter as an argument does not have any parenthesis. 

// If the function as a parameter does not have a parenthesis, the fuction as an argument should have parenthesis to denote that it is a function being passed as an argument. 
invokeFunction(argumentFunction);

// function printFullName (firstName, middleName, lastName){
//     console.log(firstName + " " + middleName + " " + lastName); 
// }

// printFullName("Juan","Dela","Cruz");

// let firstName = "Juan";
// let middleName = "Dela";
// let lastName = "Cruz";
// console.log(firstName + " " + middleName + " " + lastName);

// Return Statement
// return statement allows us to have a VALUE from a function to pass the line or block of codes that invoked/called our function - without printing it on the console

// there will be no value we can get from the function

function returnFullName (firstName, middleName, lastName){
    return firstName + " " + middleName + " " + lastName;
    // console.log("This message will not be printed"); - this line is not printed because any line/block of code after the return statement is ignored. Return ends the function execution
}
/* 
This creates a value based on the return statement in the returnFullName function returnFullName("Jeff","smith","bezos")
*/
// whatever value return, it will be the value for that function with use of the arguments passed when the function is called
console.log(returnFullName("jeff","smith","bezos"));


// the value returned from a function can also be stored inside a variable
let completeName = returnFullName("jeff","smith","bezos");
console.log(completeName);
/* 
make use of 2 parameters to show the full address of a person 
    through a return statement, create a value for the function once it is called passing 2 arguments  in the function
*/
function returnAddress(add1,add2){
    return add1 + ", " + add2;
}

console.log(returnAddress("emerald street","new york"));

function printPlayerInfo(username, level, job){
    console.log("Username: " + username);
    console.log("Level:" + level);
    console.log("Job: " + job);
}

let user1 = printPlayerInfo("knight_white", 95, "Palading");
// returns undefined because printPlayerInfo function returns nothing, its task is only to print the messages in the console but not return any value. You can't save any value from printPlayerInfo because it does not return anything.
console.log (user1); 